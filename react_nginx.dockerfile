# 1 build stage
FROM node:16-alpine AS build-stage

ARG NGINXGROUP
ARG NGINXUSER

ENV NGINXGROUP=${NGINXGROUP}
ENV NGINXUSER=${NGINXUSER}

RUN mkdir -p /app

WORKDIR /app

RUN adduser -g ${NGINXGROUP} -s /bin/sh -D ${NGINXUSER};

COPY ./frontend/package.json .
COPY ./frontend/package-lock.json .

RUN npm i

COPY ./frontend .

RUN npm run build

# 2 stage
FROM nginx:alpine

COPY --from=build-stage /app/build /usr/share/nginx/html
EXPOSE 80

CMD nginx -g 'daemon off;'
