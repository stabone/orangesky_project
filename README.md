
Project uses React, Laravel and MySQL database.


## Clone project wiht submodules

git clone --recursive https://gitlab.com/stabone/orangesky_project.git


## Frontend setup

Rename .env.example (in *frontend* direcotry) to .env, if needed REACT_APP_API_URL env. variable could be changed.
In build process this variable will be picked up and built in js assets.


## Backend setup

Rename .env.example (in *backend* directory) to .env, if want you can change MYSQL_DATABASE, MYSQL_USER, MYSQL_PASSWORD variable values.
In build process these variables will be used for mysql docker image and later for Laravel.


## Env. configuration setup

In root directory (*orangesky_project*) rename .env.example to .env


## Running project

1. cd orangesky_proj
2. *docker-compose up*
3. If needed for backend generate secret key and run migrations (that could be done using web browser)
4. In browser open localhost:8000 (if ports wasn't changed) and you should see ui part of application.
5. Backend will be on localhost:8080 (if ports wasn't changed)


## Run migrations from web browser or cli
docker exec -it *container_id* php artisan migrate


## Public ports

- MySQL 3366
- Backend 8080
- Frontend 8000
